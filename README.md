# OER

* [Kurs als Ebook](https://mastecker.gitlab.io/oer/course.epub)
* [Kurs als PDF](https://mastecker.gitlab.io/oer/course.pdf)
* [Kurs als HTML](https://mastecker.gitlab.io/oer/index.html)

