#„Open Educational Resources“
## …offene und freie Lernmate-rialien für Schule und Hochschule.
### Was sind Open Educational Resources?
Die ursprüngliche Idee von Open Educational Re-sources (im Folgenden: OER) stammt von der UNESCO, die das Ziel verfolgte, besonders den Entwicklungsländern einen freien Zugang zu Bildungsmate-rialien zu ermöglichen. 
In der Diskussion um OER liefern die Begriffe Open Content, Open Source und Open Access einen Ansatz für eine Definition von OER. Die Bewegung Open Access und Open Content zielt darauf ab, wissenschaftliche Informationen wie z.B. wissenschaftliche Publikationen, frei zur Verfügung zu stellen und damit die Forschung voranzutreiben. Die Open Source Bewegung begründet sich aus der Perspektive vieler Programmierer, vorhandene Software nach eigenen Anforderungen und Bedürfnis-sen weiterzuentwickeln. Dafür fehlte die rechtliche Erlaubnis, die die Bewegung einfordert. Mittlerweile gibt es viele Open Source Software, aber immer noch kommerzielle Programme, deren Nutzung mit Einschränkungen des Herstellers verbunden ist. Die kommerziellen Produkte decken die Bedürfnisse der großen Masse ab. Wenn man aber individuelle Anpassungen vornehmen möchte, geht dies hauptsächlich nur bei Open Source Programmen. Diese sind weitaus flexibler und können weiterentwickelt und modifiziert werden.
Zurzeit gibt es keine klare und einheitliche Definition von OER. Die OECD definiert OER wie folgt: „[...] open educational resources are digitised materials offered freely and openly for educators, students and self-learners to use und reuse for teaching, learning and research.“ (OECD, 2007, S. 30)
 Die UNSECO definiert OER als “technology-enabled, open provision of educational resources for consultation, use and adaptation by a community of users for non-commercial purposes.” (Butcher, 2011, S. 23)
Die Hewlet Foundation nimmt eine ausführlichere Definition vor: “OER als teachning, learning and research resources that reside in the public domain or have been released under an intellectual property license that permits their free use und repurposing by others. Open educational resources include full courses, course materials, modules, textbooks, streaming videos, tests, software and any other tools, materials or techniques used to support access to knowledge.” (Hewlet, 2014)
Diese Definitionen zeigen, dass es sich bei OER nicht nur um Materialien, sondern auch um Software handeln kann. Die Definitionen entscheiden sich insbesondere hinsichtlich ihrer Nutzungsgruppen und der Nutzungsrechte (wird nur von der OECD eingeschränkt). Neben den in den Definitionen benannten Charakteristika ist weiterhin von Bedeutung, dass OER formelles, informelles und lebenslanges Lernen ermöglicht und die Materialien stetig weiterentwickelt und verbessert werden können. 
Eine aktuelle Definition, die keine Einschränkungen hinsichtlich der Nutzungsrechte und –gruppen vornimmt, stammt von der Initiative „Bündnis freie Bildung“ und definiert OER als „freie Bildungsmateria-lien, d.h. Lehr- und Lernmaterialien, die frei zugänglich und dank entsprechender Lizenzierung (oder weil sie gemeinfrei sind) ohne zusätzliche Erlaubnis bearbeitet, weiterentwickelt und weitergegeben werden dürfen.“ (Bündnis Freie Bildung, 2015). 
Mit dieser Definition wird ein weiterer wichtiger Aspekt angesprochen: die Lizenzierung. Um Inhalte für die Gemeinschaft freigeben zu können, müssen diese mit einer passenden Lizenz vergeben werden. In den USA haben die AutorInnen die Möglichkeit unter einer „public domain“ ihre erstellten Inhalte generell frei zu geben und auf das Urheberrecht zu verzichten. Da man in Europa nicht auf die Urheberrechte verzichten kann, werden die Creative-Commons-Lizenzen genutzt. 

### Creative-Common-Lizenz
Die CC-Lizenzen nutzen ein dreigliedriges Modell: Es gibt (I) eine einfache kurze Beschreibung, die jeder Mensch mit Hilfe einer kurzen Beschreibung verstehen kann, zudem einen (II) juristischen Fachtext, der die exakten rechtlichen Bedingungen fest-hält und einen (III) maschinellen Text, der von Maschinen, z.B. Suchmaschinen gelesen werden kann.
Auf der Homepage www.creativecommons.org kann die jeweilige Lizenz für zu veröffentlichende Inhalte erstellt werden. Je nach Art der Lizenz kann die Verwendung der Inhalte stark eingeschränkt oder frei veröffentlicht werden. Die Lizenzierung gilt so-wohl für digitale Materialien als auch für Print-medien.

|Lizenz|Beschreibung|
|---|---|
|CC BY|Namensnennung: Diese Lizenz erlaubt die Inhalte zu kopieren, verändern, verbessern und zu verbreiten, wenn der Urheber des Originals benannt wird.|
|CC BY-SA|Namensnennung - Weitergabe nur unter gleichen Bedingungen: Diese Lizenz erlaubt die Inhalte zu kopieren, verändern, verbessern und zu verbreiten, wenn der Urheber des Originals benannt wird und die Wiederveröffentlichung unter gleichen Bedingungen läuft. Diese Lizenz wird auch von Wikipedia eingesetzt.|
|CC BY-ND|Namensnennung – keine Veränderung: Diese Lizenz erlaubt die Verwendung, wenn der Urheber des Originals benannt wird und die Inhalte nicht verän-dert werden.|
|CC BY-NC|Namensnennung – Nicht kommerziell: Diese Lizenz erlaubt die Verwendung unter der Benennung des Urhebers des Originals aber schließt kommerzielle Zwecke aus, d.h. es darf damit nicht gewirtschaftet werden.|
|CC 0|Diese Lizenz erlaubt es ein Werk zu veröffentlichen und auf alle Rechte zu verzichten und die Inhalte in eine Public Domain einzuführen|
|CC BY-NC-SA|Namensnennung – Nicht kommerziell – Weitergabe nur unter gleichen Bedingungen|
|CC BY-NC-ND|Namensnennung – Nicht kommerziell – keine Bearbeitung/Veränderung|

Egal unter welche Lizenz das Material gestellt wird, wichtig ist, dass lizenziert wird.

### OER Projekt und Initiativen
Zu der ältesten und bekanntesten OER Initiative zählt das vom Bildungsinstitut Massachusetts Institute of Technology (**MIT**) hervorgerufene Projekt OpenCourseWare (OCW, www.ocw.mit.edu), durch das seit 2002 kostenlose Materialien und Skripte ins Internet gestellt und stetig erweitert werden. Die Materialien der OCW sind lizenziert und erlauben Modifikationen und Wiederveröffentlichung mit der Bedingung, dass sie ähnlich lizenziert sind und nicht kommerziell genutzt werden.
Weiterhin hat die Open University in United Kingdom ein **Open-Learn-Projekt** gegründet, mit dem Ziel offene Bildungsmaterialien zu erstellen und mit diesen besonders ausgrenzende Bevölkerungsgrup-pen zu integrieren (www.open.edu/openlearn). 
Auf der Grundlage einer Wikiseite wurde die Plattform **Wikieducator** (http://wikieducator.org) ins Leben gerufen. Hier werden Lernmaterialien für Schulen und Hochschulen entwickelt und verbessert. Der Sitz der Organisation ist in Neuseeland.
Die Zentrale für Unterrichtsmedien im Internet e.V. (kurz **ZUM**) ist eine deutschsprachige Initiative, die sich besonders auf den Bereich Schule konzentriert. Die Plattform bietet die Möglichkeit Unterrichtsideen, -methoden und –inhalte zur Verfügung zu stellen und zu überarbeiten. Andere Beispiele sind www.lehrer-online.de oder www.unterrichtsmaterial-schule.de.
Eine noch weitere Initiative ist die im Jahr 2002 gegründete **Khan Academy** (http://khanacademy.org/), die kurze Lernvideos zur Verfügung stellt. Mittlerweile gibt es mehr als 2.500 Lernvideos, hauptsächlich im mathematisch-naturwissenschaftlichen Bereich. Damit dieses Projekt erhalten bleibt, wird es auch Google und Microsoft unterstützt.
Auch das Unternehmen Apple hat die Plattform **Itunes U** gegründet, auf der Universitäten kostenfreie Inhalte wie Audios und Videos zur Verfügung stellen können. Eine der ersten Universitäten war die Open University (UK), die bereits kurz nach dem Start 2008 mehr als 140 Kurse, 300 Alben und 2.700 Audiodateien zur Verfügung stellte. Im Jahr 2009 folge der deutschsprachige Raum. 
Dies sind nur einige wenige Beispiele, die in den letzten Jahren entstanden sind.

### Welche Potentiale stecken in OER?
Offene Bildungsressourcen bieten zahlreiche Chancen und Potentiale für die Gesellschaft und speziell für Lernende und Lehrende im universitären und schulischen Bereich. 
Ganz allgemein ist die größte Chance, dass der Zugang zur Bildung erleichtert werden kann und auch Entwicklungsländer oder nicht integrierten Bevölkerungsgruppen die Möglichkeit erhalten. Zudem bieten OER ein großes Themenspektrum und Flexibilität bei der Auswahl von Lehr- und Lernmaterialien. Da die Materialien wiederverwendet werden können, kann es zu einer deutlichen Zeit- und Kostenersparnis kommen. Dadurch, dass die Materialien stets weiterentwickelt und verbessert werden können, kann die Qualität verbessert werden. Des Weiteren wird die Bildung von Lerngruppen fördert und damit kollaborative Lernformen unterstützt. Nutzerfreundliche Software ermöglicht es in sozialen Netzwerken, Weblogs, Wikis u.v.m. in Kontakt zu treten und sich auszutauschen.

### Nutzung von OER
Nachdem die theoretischen Grundlagen und Potentiale für die Gesellschaft erläutert wurden, stellt sich die Frage wie man als Lehrender (und Lernender) OER finden, nutzen und erstellen kann.
##### OER suchen und finden
Bevor man mit der Recherche beginnt, sollten die Rahmenbedingungen stehen. Das heißt Sie sollten sich über die Zielgruppe, Zielsetzungen und Gestal-tung bereits Gedanken gemacht haben. Im Internet sind etliche vermeintlich freie Materialien. Für die Weiterbildung und den Unterricht ist es aber be-sonders wichtig, dass die Materialien frei nutzbar und entsprechend lizenziert sind. Bei den erweiter-ten Suchoptionen von **Yahoo**, **Google** und auch **YouTube** lässt sich die CC-Lizenz einstellen und filtert dementsprechend die Ergebnisse. Auch auf den Plattformen wie z.B. **creativecommons.org**, **jamendo.com** und **flickR.com** kann das Web nach lizenzfreien Materialien durchsucht werden. Dane-ben gibt es bestimmte OER-Portale und Verzeich-nisse: Diese können auf **wikieducator.org** oder **creativecommons.org/education** gesucht werden. Weiterhin gibt unter **globe-info.org** eine Meta-suchmaschine für OER.
Weitere Portale, um OER zu finden:
-	OER-Commons (nur in englischer Sprach). Link: https://www.oercommons.org/
-	ZUM-Wiki
Link: https://wikis.zum.de/
-	Elixier (durchsucht Bildungsserver)
Link: https://www.bildungsserver.de/elixier/
-	Edutags (Plattform, um OER zu finden, zu sammeln, zu verwalten)
Link: http://edutags.de/
-	Open Education Europa
Link: http://www.openeducationeuropa.eu/de

##### OER erstellen und verändern
Bei der Erstellung von OER sollten Dateiformate mit üblichen Standards verwendet werden (z.B. Bilder .png, für Texte Open-Office-Formate .html oder .xml). Es sollten solche bevorzugt werden, die das Modifizieren erleichtern (also eher eine .html Datei als eine PDF-Datei). OER können alleine aber auch gemeinsam mit anderen entwickelt werden. Dafür gibt es einige Plattformen, die die gemeinsame Bearbeitung ermöglichen: lemill.com, connexions.org, currici.org, wikiversity.org).

##### OER veröffentlichen und verwenden
Die selbst erstellen Materialien können auf einer eigenen Website oder einem eigenen Blog veröffent-licht werden. Empfehlenswert ist es aber auch die Materialien aktiv anderen zur Verfügung zu stellen und auf entsprechenden Portalen unter passenden Themenebereichen zu veröffentlichen und mit adäquaten Schlagworten zu versehen. Dabei berücksichtigt werden muss eine passende Lizenzierung, die im vorherigen Abschnitt erläutert wurde. Auf creativecommons.org kann eine entsprechende Lizenz ausgewählt werden.


#### Beispiele
- MOOC zu OER: iMOOX COER17 finden Sie unter https://imoox.at/wbtmaster/startseite/coer17.html)

- Internetseiten auf denen Fotos mit CC 0 Lizenz gefunden werden können: https://www.pexels.com/de/

- Weitere Beispiele wie OER zu finden sind, finden Sie hier: https://irights.info/artikel/wo-findet-man-freie-unterrichtsmaterialien-15-anlaufstellen-im-netz/25549


#### Quellen
- Bündnis Freie Bildung (2014). Positionspapier: Der Weg zur Stärkung freier Bildungsmaterialien. (Zugriff unter: http://buendnis-freie-bildung.de/positonspapier-oer/)
- Butcher, Neil (2011). A Basic Guide to Open Educatonal Re-sources, published by UNESCO. (Zugriff unter: http://unesdoc.unesco.org/images/0021/002158/215804e.pdf)
- Ebner, Martin & Schön, Sandra (2011). Offene Bildungsres-sourcen: Frei zugänglich und einsetzbar. In K. Wilbers & A. Hohenstein (Hrsg.), Handbuch E-Learning. Expertenwissen aus Wissenschaft und Praxis – Strategien, Instrumente, Fallstudien. (Nr. 7-15, pp. 1-14). Köln: Deutscher Wirtschaftsdienst (Wol-ters Kluwer Deutschland), 39. Erg.-Lfg. Oktober 2011.
Hewlet Foundaton (2014). Definiton of OER. (Zugriff unter: htp://www.hewlet.org/programs/edu- caton/open-educatonal-resource) 
- OECD (2007). Giving Knowledge for Free. THE EMERGENCE OF OPEN EDUCATIONAL RESOURCES. Paris. (Zugriff unter:  htp://www.oecd.org/edu/ceri/38654317.pdf) 
- Open Society Foundation (2010). Budapest: Open Access Initiative. (Zugriff unter: http://www.soros.org/openaccess/g/read.shtml)
- UNESCO (2002). Forum on the Impact of Open Courseware for Higher Education in Developing Countries. Final Report. (Zugriff unter: http://unesdoc.unesco.org/images/0012/001285/128515e.pdf)
- Stadler, Martina (2015). Was macht OER-Projekte erfolgreich? Norderstedt: Books on Demand.
- www.e-teaching.org
- https://opensource.org/docs/osd
- www.creativecommons.org

